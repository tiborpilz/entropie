/// <reference types="astro/client" />

// Path: env.d.ts

interface ImportMetaEnv {
  GHOST_CONTENT_API_URL: string;
  GHOST_CONTENT_API_KEY: string;
  SITE_URL: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
