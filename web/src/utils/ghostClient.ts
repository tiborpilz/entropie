import GhostContentAPI from '@tryghost/content-api';

export const getGhostClient = (url: string, key: string) => new GhostContentAPI({
  url,
  key,
  version: 'v5.0',
});
